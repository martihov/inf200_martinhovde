# -*- coding: utf-8 -*-

"""
Bubble sort program - sorts a list of numbers from lowest to highest value.
"""

__author__ = 'Martin Hovde'
__email__ = 'martin.hovde@nmbu.no'


def compare(a, b):
    if a > b:
        a, b = b, a
    return a, b


def bubble_sort(data):
    lis = list(data)
    list_length = len(lis)

    passes_remaining = list_length - 1
    while passes_remaining > 0 and list_length > 1:
        for i, elem in enumerate(lis[:passes_remaining]):
            lis[i], lis[i + 1] = compare(lis[i], lis[i + 1])
        passes_remaining -= 1
    return lis


if __name__ == "__main__":

    for data in ((),
                 (1,),
                 (1, 3, 8, 12),
                 (12, 8, 3, 1),
                 (8, 3, 12, 1)):
        print "{:30} --> {:30}".format(data, bubble_sort(data))
