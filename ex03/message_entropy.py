# -*- coding: utf-8 -*-

"""
This program calculates the entropy for a string.

I chose to use a variation of the function char_counts in file_stats.py, because
that counts the byte-values. The problem states to find how often
byte_values occur in strings, but converting string to lowercase, as
letter_freqs.py from ex02 does, would effectively decrease the possible entropy.

Hope this solution is adequate.
"""

__author__ = 'Martin Hovde'
__email__ = 'martin.hovde@nmbu.no'

import numpy as np


def letter_freq(text):
    result = [0] * 256
    for char in text:
        result[ord(char)] += 1
    return result


def entropy(message):
    byte_freq = letter_freq(message)
    sample_space = sum(byte_freq)

    entropy_val = 0
    if sample_space > 0:
        for i, byte_value in enumerate(byte_freq):
            p = float(byte_value) / sample_space
            if p > 0:
                entropy_val += - p * np.log2(p)
    return entropy_val


if __name__ == "__main__":
    for msg in '', 'aaaa', 'aaba', 'abcd', 'This is a short text.':
        print "{:20}: {:10.3f} bits".format(msg, entropy(msg))
