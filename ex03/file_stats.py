# -*- coding: utf-8 -*-

"""
This program counts the number of times a byte-value of a character in a file
appears, and gives the output as a list with 256 elements. The index i represent
the byte-value of the character, and the element in index i is the number of
times i appears in the file.
"""

__author__ = 'Martin Hovde'
__email__ = 'martin.hovde@nmbu.no'


def char_counts(filename):
    infile = open(filename, 'rb')
    filestr = infile.read()

    result = [0]*256
    for char in filestr:
        result[ord(char)] += 1
    return result


if __name__ == '__main__':

    fname = 'file_stats.py'
    freqs = char_counts(fname)
    for code in range(256):
        if freqs[code] > 0:
            print('{:3}{:10}'.format(code, freqs[code]))
