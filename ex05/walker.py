# -*- coding: utf-8 -*-

"""
ex05
Task B
"""

__author__ = 'Martin Hovde'
__email__ = 'martin.hovde@nmbu.no'

from random import randint


class Walker:
    """
    This class implements and simulates the walk of a (most likely) drunk
    student walking home from Samfunnet.
    """

    def __init__(self, x_initial, x_home):
        self.x, self.x_home = x_initial, x_home
        self.steps = 0

    def one_step(self):
        self.steps += 1
        self.x += (-1)**(randint(0, 1))
        return self.x

    def home(self):
        return self.x == self.x_home  # Boolean

    def current_position(self):
        return self.x

    def total_steps(self):
        return self.steps


def walk_home(initial_pos, home_pos):
    johanne = Walker(initial_pos, home_pos)
    while johanne.home() is False:
        johanne.one_step()
    return johanne.total_steps()


if __name__ == '__main__':
    dis_list = [1, 2, 5, 10, 20, 50, 100]

    for elem in dis_list:
        print 'Distance: {:3} -> Path lengths: {:30}'\
            .format(elem, [walk_home(0, elem) for _ in range(5)])
