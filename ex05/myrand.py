# -*- coding: utf-8 -*-

"""
ex05
Task A: Random generator classes
"""

__author__ = 'Martin Hovde'
__email__ = 'martin.hovde@nmbu.no'


class LCGRand:
    """
    This class is the implementation of a linear congruential generator (LCG),
    which generates numbers according to the equation

    r[n+1] = a * r[n] mod m
    """

    def __init__(self, seed):
        self.r1, self.a, self.m = seed, 7**5, 2**31 - 1

    def rand(self):
        self.r1 = (self.r1*self.a) % self.m
        return self.r1


class ListRand:
    """
    This class implements a picking of numbers from a given list.
    """

    def __init__(self, num_list):
        self.num_list = num_list
        self.i = 0

    def rand(self):
        try:
            val = self.num_list[self.i]
            self.i += 1
        except IndexError:
            raise RuntimeError('Index is out of bounds')
        return val


if __name__ == "__main__":
    """
    Initiates instances and prints some numbers from the rand() method of each.
    """

    lcg_rand = LCGRand(5)
    list_rand = ListRand([0, 4, 5, 9, 10])
    for _ in range(0, 5):
        print lcg_rand.rand()
    for _ in range(0, 5):
        print list_rand.rand()
