# -*- coding: utf-8 -*-

__author__ = 'Martin Hovde'
__email__ = 'martihov@nmbu.no'

'''
This program counts the number of times a character is used in a string given by
the user and prints this to terminal.

Side note: The warning on line 27 seems to be a bug in pycharm's inspection,
according to http://stackoverflow.com/questions/33738452/weak-warning-in-pycharm
-unexpected-argument
'''

import collections


def letter_freq(text):
    original_string = text
    lowercase_string = original_string.lower()
    freqs_dictionary = {}
    for i in range(len(lowercase_string)):
        if lowercase_string[i] not in freqs_dictionary:
            freqs_dictionary[lowercase_string[i]] = 1
        else:
            freqs_dictionary[lowercase_string[i]] += 1

    # We need OrderedDict to create a new, ordered, dictionary, as the built-in
    # .items() creates tuples. Sorting these with built-in sorted() does not
    # produce a dictionary. Therefore, one needs to construct the dictionary
    # again from the list of tuples.  That is, if one wants to have
    # the output as a dictionary, instead of a list consisting of tuples.

    ordered_dictionary = collections.OrderedDict(sorted(freqs_dictionary.items()
                                                        ))
    return ordered_dictionary


if __name__ == '__main__':
    text = raw_input('Please enter text to analyse: ')

    freqs = letter_freq(text)
    for letter, count in freqs.items():
        print('{:3}{:10}'.format(letter, count))
