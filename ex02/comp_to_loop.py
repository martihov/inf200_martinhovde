# -*- coding: utf-8 -*-

__author__ = 'Martin Hovde'
__email__ = 'martihov@nmbu.no'

'''
This program creates a certain defined list in two different ways:
1) List comprehension
2) Loop
'''


def squares_by_comp(n):
    return [k**2 for k in range(n) if k % 3 == 1]


def squares_by_loop(n):
    square_list = []
    for k in range(n):
        if k % 3 == 1:
            square_list.append(k ** 2)
    return square_list


if __name__ == '__main__':
    N = 17
    if squares_by_comp(N) != squares_by_loop(N):
        print "ERROR!"