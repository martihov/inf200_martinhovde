# -*- coding: utf-8 -*-

__author__ = 'Martin Hovde'
__email__ = 'martihov@nmbu.no'

'''
This code is a more tidy version of the code on BitBucket, with more
explaining variable names.

This game simulates the throw of two dice, generating a random integer (no. of
eyes) between 2 and 12, and the player is to guess on the number of eyes.
The player has 3 tries. The amounts of points won is dependent on how many
guesses were needed. If wrong 3 times, the game ends.
'''


from random import randint


def dice_throw():
    return randint(1, 6) + randint(1, 6)


def get_player_guess():
    get_guess = 0
    while get_guess < 1:
        get_guess = int(raw_input('Your guess: '))
    return get_guess


def check_if_two_numbers_identical(a, b):
    return a == b


if __name__ == '__main__':

    guess_correct = False
    guesses_left = 3
    number_of_eyes = dice_throw()

    while not guess_correct and guesses_left > 0:
        guess = get_player_guess()
        guess_correct = check_if_two_numbers_identical(number_of_eyes, guess)
        if not guess_correct:
            print('Wrong, try again!')
            guesses_left -= 1

    if guesses_left > 0:
        print('You won {} points.'.format(guesses_left))
    else:
        print('You lost. Correct answer: {}.'.format(number_of_eyes))
