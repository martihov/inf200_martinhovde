# -*- coding: utf-8 -*-

"""
Test algorithms for bubble_sort.py
"""

__author__ = 'Martin Hovde'
__email__ = 'martin.hovde@nmbu.no'

import nose.tools as nt
from bubble_sort import bubble_sort as bs


def test_empty():
    """Test that the sorting function works for empty list"""
    assert bs([]) == [], 'Failed test, program should return empty list.'


def test_single():
    """Test that the sorting function works for single-element list"""
    assert bs([3]) == [3], 'Failed test, program did not return correct list.'


def test_sorted_is_not_original():
    """Test that the sorting function returns a new object."""
    a = [1, 2]
    assert bs([a]) is not a, 'Failed test, sorting function references ' \
                             'original.'


def test_original_unchanged():
    """Test that sorting leaves the original data unchanged."""
    a = [3, 2, 1]
    b = bs(a)
    assert a == [3, 2, 1], 'Failed test, sorting does not leave original ' \
                           'data unchanged.'


def test_sort_sorted():
    """Test that sorting works on sorted data."""

    nt.assert_true(bs([1, 2, 3]) == [1, 2, 3], 'Failed test, does not work on'
                                               ' sorted data.')


def test_sort_reversed():
    """Test that sorting works on reverse-sorted data."""
    nt.assert_true(bs([3, 2, 1]) == [1, 2, 3], 'Failed test, did not work on'
                                               ' reverse-sorted data')


def test_sort_allequal():
    """Test that sorting handles data with identical elements."""
    nt.assert_true(bs([2, 2, 2]) == [2, 2, 2], 'Failed test, did not work on'
                                               ' data with identical elements')


def test_sorting():
    """Test sorting for various test cases."""
    nt.assert_true(bs([3, 2, 8, 12, 5]) == [2, 3, 5, 8, 12], 'Failed test, did'
                                                             ' not give correct'
                                                             ' results.')
    nt.assert_true(bs([3, 2, 2, 4]) == [2, 2, 3, 4], 'Failed test, did not give'
                                                     ' correct results.')
    nt.assert_true(bs([12, 1, 1]) == [1, 1, 12], 'Failed test, did not give'
                                                 ' correct results')
