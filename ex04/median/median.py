# -*- coding: utf-8 -*-

"""
Finds the median of data.

Program code is based on code from the course INF200, presumably written by
Jonas van den Brink.
"""

__author__ = 'Martin Hovde'
__email__ = 'martin.hovde@nmbu.no'


def median(data):
    """
    Returns median of data.

    :param data: An iterable of containing numbers
    :return: Median of data
    """

    sdata = sorted(data)
    n = len(sdata)
    if n == 0:
        raise ValueError('Median of empty list not defined')
    return (sdata[n/2] if n % 2 == 1
            else 0.5 * (sdata[n/2 - 1] + sdata[n/2]))
