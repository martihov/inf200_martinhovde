# -*- coding: utf-8 -*-

"""
Test program for median.py.
"""

__author__ = 'Martin Hovde'
__email__ = 'martin.hovde@nmbu.no'

from median import median as md
import nose.tools as nt


def test_one_element_list():
    """Test that the median function works for one-element lists"""
    nt.assert_true(md([1]) == 1, 'Test failed')


def test_odd_number_list():
    """Test that the median function works for odd numbered lists."""
    nt.assert_true(md([1, 2, 3]) == 2, 'Test failed')


def test_even_number_list():
    """Test that the median function works for even numbered lists."""
    nt.assert_true(md([1, 2, 3, 4]) == 2.5, 'Test failed')


def test_ordered_list():
    """Test that the median function works for ordered lists."""
    nt.assert_true(md([1, 3, 5, 8]) == 4, 'Test failed')


def test_reverse_ordered_list():
    """Test that the median function works for reverse-ordered lists."""
    nt.assert_true(md([8, 7, 5, 3]) == 6, 'Test failed')


def test_unordered__list():
    """Test that the median function works for unordered lists."""
    nt.assert_true(md([3, 8, 1, 9, 5]) == 5, 'Test failed')


def test_empty_list():
    """Test that the median function raises a ValueError for empty lists."""
    nt.assert_raises(ValueError, md, [])


def test_original_unchanged():
    """Test that the median function leaves the original list unchanged."""
    a = [5, 4, 3, 1]
    b = md(a)
    nt.assert_true(a == [5, 4, 3, 1], 'Test failed.')


def test_tuples():
    """Test that the median function works for tuples."""
    nt.assert_true(md((1, 2, 3)) == 2, 'Test failed.')
